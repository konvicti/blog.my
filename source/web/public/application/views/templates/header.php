<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title ?> </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="../../../html/css/bootstrap.min.css">

        <link href="../../../html/lightbox/css/lightbox.css" rel="stylesheet" />
        <link rel="stylesheet" href="../../../html/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../../../html/css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="../../../js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 padr0 padl0">
                    <div class="blog__menu">
                        <img src="../../../html/img/blog_name.png" alt="blog" class="center-block">
                        <ul>
                            <li class="<?= $menu == 'Blog' ? 'active' : '' ?>"><a href="/">blog</a></li>
                            <li class="<?= $menu == 'about' ? 'active' : '' ?>"><a href="/about">about me</a></li>
                            <li class="<?= $menu == 'gallery' ? 'active' : '' ?>"><a href="/gallery">gallery</a></li>
                            <li class="<?= $menu == 'contact' ? 'active' : '' ?>"><a href="/contact">contact</a></li>
                            <li class="<?= $menu == 'catalog' ? 'active' : '' ?>"><a href="/catalog">catalog</a></li>
                        </ul>
                        <div class="clear"></div>
                        <p id="tah_status-heading">Weekly working time</p>
                        <p id="tah_status-text"><span id="tah_status"></span> hours</p>
                    </div>
                </div>