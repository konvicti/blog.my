<div class="row">
    <div class="col-md-12 padr0">
        <div class="footer">
            <div class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-4">
                            <p class="footer__heading text-uppercase">Morbi accumsan ipsum velit </p>
                            <div class="footer__dashed"></div>
                            <p class="footer__text">Sed non  mauris vitae erat consequat auctor eu 
                                in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
                                himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. </p>
                            <p class="footer__text">Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, 
                                erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. </p>
                        </div>
                        <div class="col-md-offset-1 col-md-3">
                            <p class="footer__heading text-uppercase">Quick Links</p>
                            <div class="footer__dashed"></div>
                            <p class="footer__text"><span class="footer--span">Link Here</span><br>
                                Lorem Ipsum Dolor Sit Amet</p>
                            <p class="footer__text"><span class="footer--span">Link Here</span><br>
                                Lorem Ipsum Dolor Sit Amet</p>
                            <p class="footer__text"><span class="footer--span">Link Here</span><br>
                                Lorem Ipsum Dolor Sit Amet</p>
                        </div>
                        <div class="col-md-offset-1 col-md-3">
                            <p class="footer__heading text-uppercase">Quick Links</p>
                            <div class="footer__dashed"></div>
                            <p class="footer__text"><span class="footer--span">Link Here</span><br>
                                Lorem Ipsum Dolor Sit Amet</p>
                            <p class="footer__text"><span class="footer--span">Link Here</span><br>
                                Lorem Ipsum Dolor Sit Amet</p>
                            <p class="footer__text"><span class="footer--span">Link Here</span><br>
                                Lorem Ipsum Dolor Sit Amet</p>
                        </div>
                    </div>                                         
                </div>
            </div>
        </div>
        <div class="footer__copyright">
            <div class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-2">
                            <span>&copy 2012 Blog Name.</span>
                        </div>
                        <div class="col-md-offset-7 col-md-2">
                            <a href="http://facebook.com"><img src="../../../html/img/facebook.png" alt="" class="center-block"></a>
                            <a href="http://twitter.com"><img src="../../../html/img/twitter.png" alt="" class="center-block"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../../../../html/js/vendor/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
    <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
    <script src="../../../html/lightbox/js/jquery-1.11.0.min.js"></script>
    <script src="../../../html/lightbox/js/lightbox.min.js"></script>
    <script src="../../../../html/js/main.js"></script>
    </body>
</html>