<div class="col-md-10 padl0 padr0">
    <div class="fullcontent">
        <?php if (isset($user_name)): ?>
        <?php if ($user_rights==false): ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php else: ?>
        <p class="user_loged_in">You loged in like - <b><?php echo $user_name ?></b> (<a href="/users/log_out">Log out</a>)</p>
        <?php endif; ?>
        <?php else: ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php endif; ?>
        <div class="row margr0">
            <div class="col-md-7 padr0">
                <div class="content">
                    <p class="gallery__content--heading">Contact</p>
                    <p class="about__content--text">Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, 
                        velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
                    <form class="form-horizontal" action="/message" method="post">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="First Name" name="firstname">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Last Name" name="lastname">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                              <input type="tel" class="form-control" placeholder="Phone number" name="phone">
                            </div>
                            <div class="col-sm-6">
                              <input type="email" class="form-control" placeholder="Email address" name="email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <textarea class="form-control" rows="3" placeholder="Message / question" name="message" id="contact_textarea"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-5 padl30">
                <div class="contact__phone-mail margphone">
                    <img src="../../../html/img/phone.png" alt="phone">
                    <p class="contact__text">Phone:<span class="contact__span-black">123.456.7890</span></p>   
                </div>
                <div class="contact__phone-mail">
                    <img src="../../../html/img/sobaka.png" alt="sobaka">
                    <p class="contact__text">Email:<a href="#">info@damain.com</a></p>
                </div>
            </div>
        </div>
        
<!-- FORM AJAX  -->                  
<div class="login_hidden">
    <form class="form-horizontal" id="user_log_form" method="post">
        <div class="form-group">
            <label for="login" class="col-sm-offset-1 col-sm-2 control-label">Login</label>
            <div class="col-sm-6">
                <input type="text" id="login" class="login form-control ntSaveForms" name="login">
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="col-sm-offset-1 col-sm-2 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" id="pass" class="password form-control" name="password">
            </div>
        </div>
        <button class="login_button btn btn-default center-block" id="login_button">Log In</button><br>                                
    </form>
    <button class="btn btn-default center-block" id="hide_button">Hide</button>
</div>        