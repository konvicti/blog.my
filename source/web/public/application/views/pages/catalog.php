<div class="col-md-10 padl0 padr0">
    <div class="fullcontent">
        <?php if (isset($user_name)): ?>
        <?php if ($user_rights==false): ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php else: ?>
        <p class="user_loged_in">You loged in like - <b><?php echo $user_name ?></b> (<a href="/users/log_out">Log out</a>)</p>
        <?php endif; ?>
        <?php else: ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-7 padr0">
                <div class="content">
                    <?php foreach ($xml as $book): ?>
                        <div class="col-lg-4">
                            <div class="gallery__imgcontainer">
                                <h1 class="books_heading"><?php echo $book->name ?></h1>
                                <p class="books_text"><span class="books_span">Author: </span><?php echo $book->author ?></p>
                                <p class="books_text"><span class="books_span">Genre: </span><?php echo $book->genre ?></p>
                                <p class="books_text"><span class="books_span">Pages: </span><?php echo $book->pages ?></p>
                                <p class="books_text"><span class="books_span">Year: </span><?php echo $book->year ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        
<!-- FORM AJAX  -->                  
<div class="login_hidden">
    <form class="form-horizontal" id="user_log_form" method="post">
        <div class="form-group">
            <label for="login" class="col-sm-offset-1 col-sm-2 control-label">Login</label>
            <div class="col-sm-6">
                <input type="text" id="login" class="login form-control ntSaveForms" name="login">
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="col-sm-offset-1 col-sm-2 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" id="pass" class="password form-control" name="password">
            </div>
        </div>
        <button class="login_button btn btn-default center-block" id="login_button">Log In</button><br>                                
    </form>
    <button class="btn btn-default center-block" id="hide_button">Hide</button>
</div>                

