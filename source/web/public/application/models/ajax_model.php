<?php

class Ajax_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function admin_login($data) {
        $this->db->where('login',$data['log']);
        $query = $this->db->get('admin');
        return $query->result_array();
    }

    public function user_login($data) {
        $this->db->where('login',$data['log']);
        $query = $this->db->get('users');
        return $query->result_array();
    }
    
    public function user_email($data) {
        $this->db->where('email',$data['email']);
        $query = $this->db->get('users');
        return $query->result_array();
    }
};


