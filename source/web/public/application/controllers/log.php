<?php

class Log extends CI_Controller {

    public function index()
    {
        if (isset($this->session->userdata['is_loged_in'])>0) {
            redirect('/admin');
        }
        $data['title'] = 'Log In - Admin';
        $this->load->view('admin/log_view',$data);
    }
    
    function validate_credentials()
    {
        $data['login'] = $this->input->post('login');
        $data['password'] = $this->input->post('password');
        $this->load->model('log_model');
        $query = $this->log_model->admin_validate($data);
        $flag=false;
        foreach ($query as $row)
        {
            if ($row['login']=$data['login'] && $row['password']=$data['password']) {
                $flag=true;
            }
        }
        if ($flag)
        {
            $data = array(
                'username' => $this->input->post('login'),
                'is_loged_in' => 15
            );
            $this->session->set_userdata($data);
            redirect('/admin');
        }
        else {
            $this->index();
        }
    }
    
}