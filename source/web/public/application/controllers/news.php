<?php

class News extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('news_model');
    }

    public function index() {
        
        if (isset($this->session->userdata['is_loged_in'])) {
        $data['user_name'] = $this->session->userdata['username'];
        if ($this->session->userdata['is_loged_in']==1 || $this->session->userdata['is_loged_in']==15) {
            $data['user_rights'] = true;
        }
        else
        { 
            $data['user_rights'] = false;
        }}
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'blog/page';
        $config['total_rows'] = $this->db->count_all('blog_news');
        $config['per_page'] = 2;
        $config['num_links'] = 3;
        $config['full_tag_open'] = '<ul class="list-inline">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';
        $config['last_link'] =false;
        $config['next_link'] =false;
        $config['prev_link'] = false;
        $config['first_link'] =false;

        $this->pagination->initialize($config);

        $data['news'] = $this->news_model->get_news($config['per_page'], 0);
        $data['title'] = ucfirst('blog');
        $data['menu'] = $data['title'];
        $this->load->view('templates/header', $data);
        $this->load->view('news/blog', $data);
        $this->load->view('templates/footer');
    }

    public function page($offset = false) {
        if (isset($this->session->userdata['is_loged_in'])) {
        $data['user_name'] = $this->session->userdata['username'];
        if ($this->session->userdata['is_loged_in']==1 || $this->session->userdata['is_loged_in']==15) {
            $data['user_rights'] = true;
        }
        else
        { 
            $data['user_rights'] = false;
        }}
        if(!isset($offset) || $offset == ''){
            redirect(base_url());
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'blog/page';
        $config['total_rows'] = $this->db->count_all('blog_news');
        $config['per_page'] = 2;
        $config['num_links'] = 3;
        $config['full_tag_open'] = '<ul class="list-inline">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';
        $config['last_link'] =false;
        $config['next_link'] =false;
        $config['prev_link'] = false;
        $config['first_link'] =false;

        $this->pagination->initialize($config);

        $data['news'] = $this->news_model->get_news($config['per_page'], $offset);
        $data['title'] = ucfirst('blog');
        $data['menu'] = $data['title'];
        $this->load->view('templates/header', $data);
        $this->load->view('news/blog', $data);
        $this->load->view('templates/footer');
    }

    public function test($slug) {
        if (isset($this->session->userdata['is_loged_in'])) {
        $data['user_name'] = $this->session->userdata['username'];
        if ($this->session->userdata['is_loged_in']==1 || $this->session->userdata['is_loged_in']==15) {
            $data['user_rights'] = true;
        }
        else
        { 
            $data['user_rights'] = false;
        }}
        $data['news_item'] = $this->news_model->get_each($slug);

        if (empty($data['news_item'])) {
            show_404();
        }

        $data['title'] = ucfirst('blog');
        $data['menu'] = $data['title'];

        $data['id']=$data['news_item']['id'];
        $data['comment'] = $this->news_model->show_comment($data);
        
        $this->load->view('templates/header', $data);
        $this->load->view('news/news_each', $data);
        $this->load->view('templates/footer');

        
    }
    
    public function add_comment() {
        if (isset($this->session->userdata['is_loged_in'])) {
        $data['user_name'] = $this->session->userdata['username'];
        if ($this->session->userdata['is_loged_in']==1 || $this->session->userdata['is_loged_in']==15) {
            $data['user_rights'] = true;
        }
        else
        { 
            $data['user_rights'] = false;
        }}
        $data['text'] = $this->input->post('text');
        $data['slug'] = $this->input->post('slug');
        $data['id_news'] = $this->input->post('id_news');
        $this->load->helper('form');
	$this->load->library('form_validation');
	$this->form_validation->set_rules('text', 'text', 'required');
        if ($this->form_validation->run() === FALSE)
	{
            redirect('blog/'.$data['slug']);
	}
	else
	{
            $this->load->model('news_model');
            $this->news_model->add_comment($data);
            redirect('blog/'.$data['slug']);
	}
    }
    
    
    
}
