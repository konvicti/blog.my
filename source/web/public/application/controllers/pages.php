<?php

class Pages extends CI_Controller {

    public function view($page) {
        if (isset($this->session->userdata['is_loged_in'])) {
        $data['user_name'] = $this->session->userdata['username'];
        if ($this->session->userdata['is_loged_in']==1 || $this->session->userdata['is_loged_in']==15) {
            $data['user_rights'] = true;
        }
        else
        { 
            $data['user_rights'] = false;
        }}
        if (!file_exists('application/views/pages/' . $page . '.php')) {
            // Упс, у нас нет такой страницы!
            show_404();
        }
        $data['title'] = ucfirst($page); // Сделать первую букву заглавной
        $data['menu'] = lcfirst($page);
        $html = array(
            'header' => $this->load->view('templates/header', $data, true),
            'content' => $this->load->view('pages/' . $page, $data, true),
            'footer' => $this->load->view('templates/footer', $data, true)
        );
        
        $this->load->view('templates/index', $html);
            
    }
   
    public function gallery() {
        if (isset($this->session->userdata['is_loged_in'])) {
        $data['user_name'] = $this->session->userdata['username'];
        if ($this->session->userdata['is_loged_in']==1 || $this->session->userdata['is_loged_in']==15) {
            $data['user_rights'] = true;
        }
        else
        { 
            $data['user_rights'] = false;
        }
        if ($this->session->userdata['is_loged_in']==15) {
            $data['admin_rights'] = true;
        }
        else
        { 
            $data['admin_rights'] = false;
        }
        }
        $data['title'] = "Gallery"; // Сделать первую букву заглавной
        $data['menu'] = "gallery";
        
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'gallery/page';
        $config['total_rows'] = $this->db->count_all('gallery');
        $config['per_page'] = 2;
        $config['num_links'] = 3;
        $config['full_tag_open'] = '<ul class="list-inline">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';
        $config['last_link'] =false;
        $config['next_link'] =false;
        $config['prev_link'] = false;
        $config['first_link'] =false;
        
        $this->pagination->initialize($config);
        $this->load->model('news_model');
        $data['gallery'] = $this->news_model->get_gallery($config['per_page'], 0);
        
        $this->load->view('templates/header', $data);
        $this->load->view('pages/gallery', $data);
        $this->load->view('templates/footer');
    }
    
    public function page($offset=false) {
        if (isset($this->session->userdata['is_loged_in'])) {
        $data['user_name'] = $this->session->userdata['username'];
        if ($this->session->userdata['is_loged_in']==1 || $this->session->userdata['is_loged_in']==15) {
            $data['user_rights'] = true;
        }
        else
        { 
            $data['user_rights'] = false;
        }
        if ($this->session->userdata['is_loged_in']==15) {
            $data['admin_rights'] = true;
        }
        else
        { 
            $data['admin_rights'] = false;
        }
        }
        if(!isset($offset) || $offset == ''){
            redirect('gallery');
        }
        $data['title'] = "Gallery"; // Сделать первую букву заглавной
        $data['menu'] = "gallery";
        
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'gallery/page';
        $config['total_rows'] = $this->db->count_all('gallery');
        $config['per_page'] = 2;
        $config['num_links'] = 3;
        $config['full_tag_open'] = '<ul class="list-inline">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';
        $config['last_link'] =false;
        $config['next_link'] =false;
        $config['prev_link'] = false;
        $config['first_link'] =false;
        $data['add_photo'] = false;
        
        if ($offset==ceil($config['total_rows']/$config['per_page'])+1) {
            $data['add_photo']=true;
        }
        $this->pagination->initialize($config);
        $this->load->model('news_model');
        $data['gallery'] = $this->news_model->get_gallery($config['per_page'], $offset);
        $this->load->view('templates/header', $data);
        $this->load->view('pages/gallery', $data);
        $this->load->view('templates/footer');
    }
    
    public function catalog() {
        $data['title'] = "Catalog"; // Сделать первую букву заглавной
        $data['menu'] = "catalog";
        
        $data['xml'] = simplexml_load_file("books.xml");  
        $this->load->view('templates/header', $data);
        $this->load->view('pages/catalog', $data);
        $this->load->view('templates/footer');
    }

}
