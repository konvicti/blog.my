<div class="col-md-10 padl0 padr0">
    <div class="fullcontent">
        <?php if (isset($user_name)): ?>
        <?php if ($user_rights==false): ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php else: ?>
        <p class="user_loged_in">You loged in like - <b><?php echo $user_name ?></b> (<a href="/users/log_out">Log out</a>)</p>
        <?php endif; ?>
        <?php else: ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php endif; ?>
        <div class="row margr0">
            <div class="col-md-7 padr0">
                <div class="content margb28">
                    <div class="content__img">
                        <p class="about__content-img--heading">About Me</p>
                        <p class="about__content-img--text-H text-uppercase">Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt</p>
                        <p class="about__content-img--text text-uppercase">tincidunt auctor a ornare odio. Sed non  mauris vitae 
                            erat consequat auctor eu in elit. Class aptent taciti sociosqu 
                            ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>
                        <div class="clear"></div>
                    </div>
                    <p class="about__content--heading">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos</p>
                    <p class="about__content--text">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec 
                        sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. 
                        Nam nec tellus a odio tincidunt auctor a ornare odio. </p>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="about__content-rows--heading">Sed non  mauris vitae erat consequat </p>
                            <div class="about__dashed"></div>
                            <p class="about__content-rows--text">Morbi accumsan ipsum velit. 
                                Nam nec tellus a odio tincidunt auctor a ornare odio. 
                                Sed non  mauris vitae erat consequat auctor eu in elit. </p>
                            <p class="about__content-rows--text">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                            <p class="about__content-rows--text">Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. 
                                Sed non neque elit. Sed ut imperdiet nisi. </p>
                            <p class="about__content-rows--text">Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit 
                                mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
                            <p class="about__content-rows--text">Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. 
                                Sed non  mauris vitae erat consequat auctor eu in elit. </p>
                            <p class="about__content-rows--text">Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                        </div>
                        <div class="col-md-6">
                            <p class="about__content-rows--heading">Sed non  mauris vitae erat consequat auctor</p>
                            <div class="about__dashed"></div>
                            <p class="about__content-rows--text"><span class="about__span--black">Morbi accumsan ipsum velit.</span> Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris 
                                vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent 
                                per conubia nostra, per inceptos himenaeos. </p>
                            <p class="about__content-rows--text"><span class="about__span--black">Mauris in erat justo.</span> Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi.
                                Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
                            <p class="about__content-rows--text"><span class="about__span--black">Morbi accumsan ipsum velit.</span> Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris 
                                vitae erat consequat auctor eu in elit.<br>
                                Class aptent <span class="about__span--orange">taciti sociosqu ad litora</span> torquent per conubia nostra, per inceptos himenaeos. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padl30">
                <p class="about__posts--heading text-uppercase">latest posts</p>
                <div class="about__month">
                    <div class="row">
                        <div class="col-md-1">
                            <div id="circle"></div>
                        </div>
                        <div class="col-md-10">
                            <span>January 8th, 2013</span>
                            <p class="about__content-rows--text"><span class="about__span--orange">Nam nec tellus a odio tinciduntX auctor a ornare odio.</span><br>
                                Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti ...</p>
                        </div>
                    </div>
                </div>
                <div class="about__month">
                    <div class="row">
                        <div class="col-md-1">
                            <div id="circle"></div>
                        </div>
                        <div class="col-md-10">
                            <span>January 8th, 2013</span>
                            <p class="about__content-rows--text"><span class="about__span--orange">Nam nec tellus a odio tinciduntX auctor a ornare odio.</span></p>
                        </div>
                    </div>
                </div>
                <div class="about__month">
                    <div class="row">
                        <div class="col-md-1">
                            <div id="circle"></div>
                        </div>
                        <div class="col-md-10">
                            <span>January 8th, 2013</span>
                            <p class="about__content-rows--text"><span class="about__span--orange">Nam nec tellus a odio tinciduntX auctor a ornare odio.</span><br>
                                Sed non  mauris vitae erat...</p>
                        </div>
                    </div>
                </div>
                <div class="about__month">
                    <div class="row">
                        <div class="col-md-1">
                            <div id="circle"></div>
                        </div>
                        <div class="col-md-10">
                            <span>January 8th, 2013</span>
                            <p class="about__content-rows--text"><span class="about__span--orange">Nam nec tellus a odio tinciduntX auctor a ornare odio.</span><br>
                                Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti ...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- FORM AJAX  -->                  
<div class="login_hidden">
    <form class="form-horizontal" id="user_log_form" method="post">
        <div class="form-group">
            <label for="login" class="col-sm-offset-1 col-sm-2 control-label">Login</label>
            <div class="col-sm-6">
                <input type="text" id="login" class="login form-control ntSaveForms" name="login">
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="col-sm-offset-1 col-sm-2 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" id="pass" class="password form-control" name="password">
            </div>
        </div>
        <button class="login_button btn btn-default center-block" id="login_button">Log In</button><br>                                
    </form>
    <button class="btn btn-default center-block" id="hide_button">Hide</button>
</div>                        