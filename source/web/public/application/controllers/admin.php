<?php

class Admin extends CI_Controller {

    public function index()
    {
        if (isset($this->session->userdata['is_loged_in'])) {
        if (($this->session->userdata['is_loged_in'])<15) {
            $data['title'] = 'Log In - Admin';
            $this->load->view('admin/log_view',$data);
        }
        else
        {
            $data['title'] = 'Admin';
            $this->load->view('admin/index',$data);
        }}
        else
        {
            $data['title'] = 'Log In - Admin';
            $this->load->view('admin/log_view',$data);
        }
        
    }
    
    function add_news()
    {
        if (($this->session->userdata['is_loged_in'])<15) {
            redirect('/log');
        }
        $this->load->helper('form');
	$this->load->library('form_validation');

	$data['title'] = 'Create a news item';
        $data['menu'] = 'sdgsdg';
	$this->form_validation->set_rules('title', 'title', 'required');
	$this->form_validation->set_rules('text', 'text', 'required');

	if ($this->form_validation->run() === FALSE)
	{
                $data['flag'] = false;
		$this->load->view('admin/add_news_view',$data);

	}
	else
	{
                $data['flag'] = true;
                $this->load->model('news_model');
		$this->news_model->add_news();
		$this->load->view('admin/add_news_view',$data);
	}
        
    }
    
    function change_news()
    {
        if (($this->session->userdata['is_loged_in'])<15) {
            redirect('/log');
        }
        
        $this->load->model('news_model');
        $data['news'] = $this->news_model->get_news_to_del();
	$data['title'] = 'Change or delete news';
        $this->load->view('admin/change_news_view',$data);        
        
    }
    
    function delete_news()
    {
        if (($this->session->userdata['is_loged_in'])<15) {
            redirect('/log');
        }
        $this->load->model('news_model');
        if (empty($_GET["slug"])) {
                echo "Error!";
                include "del_news";
                exit;
        }
        $data['slug'] = $_GET["slug"];
        $this->news_model->del_news($data);
        $data['news'] = $this->news_model->get_news_to_del();
	$data['title'] = 'Delete a news item';
        $this->load->view('admin/change_news_view',$data);
    }
    
        function edit_news()
    {
        if (($this->session->userdata['is_loged_in'])<15) {
            redirect('/log');
        }
        if (empty($_GET["slug"])) {
                echo "Error!";
                exit;
        }
        $this->load->helper('form');
	$this->load->library('form_validation');
        
        $data['slug'] = $_GET["slug"];
	$data['title'] = 'Edit news';
        
        $this->load->model('news_model');
        if (count($this->news_model->get_edit_news($data))==1) {
            $data['news'] = $this->news_model->get_edit_news($data);
            $this->load->view('admin/edit_news_view',$data);
            fb($data['news']);
        }
        else {
            $this->load->view('admin/change_news_view',$data);
        }
        
	
    }
    
    function changed_news()
    {
        if (($this->session->userdata['is_loged_in'])<15) {
            redirect('/log');
        }
        $data['id'] = $this->input->post('id_news');
        $data['titled'] = $this->input->post('title');
        $data['text'] = $this->input->post('text');
        
        $this->load->model('news_model');
        $this->news_model->changed_news($data);
        $data['news'] = $this->news_model->get_news_to_del();
        $data['title'] = 'Change or delete news';
        $this->load->view('admin/change_news_view',$data);
    }
    
    function log_out()
    {
        $data = array(
                'is_loged_in' => 14
            );
            $this->session->set_userdata($data);
            redirect('/log');
    }
    
    function add_photo()
    {
        if (($this->session->userdata['is_loged_in'])<15) {
            redirect('/log');
        }
        $this->load->helper('form');
	$this->load->library('form_validation');
        
        $config['total_rows'] = $this->db->count_all('gallery');
        $data['num_page'] = ceil($config['total_rows']/2+1);
	$data['title'] = 'Add new photo';
        $data['menu'] = 'sdgsdg';
	$this->form_validation->set_rules('title', 'title', 'required');
	$this->form_validation->set_rules('text', 'text', 'required');

	if ($this->form_validation->run() === FALSE)
	{
            $data['flag'] = false;
            $this->load->view('admin/add_photo_view',$data);

	}
	else
	{
            $data['flag'] = true;
            $this->load->model('news_model');
            $this->news_model->add_photo();
            redirect('/gallery');
	}
        
    }

}