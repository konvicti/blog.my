<div class="col-md-10 padl0 padr0">
    <div class="fullcontent">
        <?php if (isset($user_name)): ?>
        <?php if ($user_rights==false): ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php else: ?>
        <p class="user_loged_in">You loged in like - <b><?php echo $user_name ?></b> (<a href="/users/log_out">Log out</a>)</p>
        <?php endif; ?>
        <?php else: ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php endif; ?>
        <?php foreach ($news as $news_item): ?>
        <div class="row">
            <div class="col-md-7 padr0">
                <div class="content">
                    <p class="content--heading"><?php echo $news_item['title'] ?></p>
                    <?php if (!empty($news_item['img'])): ?>
                    <img src="<?php echo $news_item['img'] ?>"  alt="Responsive image" class="img-responsive center-block">
                    <?php endif; ?>
                    <p class="content--text"><?php echo $news_item['prev'] ?></p>
                    <a href="/blog/<?php echo $news_item['slug'] ?>"><button class="btn btn-warning">Continue Reading</button></a>
                </div>
            </div>
            <div class="col-md-3 padl0">
                <div class="month">
                    <div id="circle"></div>
                    <span>January 8th, 2013</span>
                </div>
            </div>
        </div>
 

        <?php endforeach ?>

        <div class="row">
            <div class="col-md-7 padr0">
                <div class="content">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>  

<!-- FORM AJAX  -->                  
<div class="login_hidden">
    <form class="form-horizontal" id="user_log_form" method="post">
        <div class="form-group">
            <label for="login" class="col-sm-offset-1 col-sm-2 control-label">Login</label>
            <div class="col-sm-6">
                <input type="text" id="login" class="login form-control ntSaveForms" name="login">
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="col-sm-offset-1 col-sm-2 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" id="pass" class="password form-control" name="password">
            </div>
        </div>
        <button class="login_button btn btn-default center-block" id="login_button">Log In</button><br>                                
    </form>
    <button class="btn btn-default center-block" id="hide_button">Hide</button>
</div>        