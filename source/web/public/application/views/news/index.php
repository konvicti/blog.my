<div class="col-md-10 padl0 padr0">
    <div class="fullcontent">
        <?php foreach ($news as $news_item): ?>
        <div class="row">
            <div class="col-md-7 padr0">
                <div class="content">
                    <p class="content--heading"><?php echo $news_item['title'] ?></p>
                    <img src="<?php echo $news_item['img'] ?>"  alt="Responsive image" class="img-responsive center-block">
                    <p class="content--text"><?php echo $news_item['text'] ?></p>
                    <a href="news/<?php echo $news_item['slug'] ?>"><button class="btn btn-warning">Continue Reading</button></a>
                </div>
            </div>
            <div class="col-md-3 padl0">
                <div class="month">
                    <div id="circle"></div>
                    <span>January 8th, 2013</span>
                </div>
            </div>
        </div>
 

<?php endforeach ?>

        <div class="row">
            <div class="col-md-7 padr0">
                <div class="content last">
                    <button type="button" class="btn btn-primary btnactive">1</button>
                    <button type="button" class="btn btn-primary">2</button>
                    <button type="button" class="btn btn-primary">3</button>
                    <button type="button" class="btn btn-primary">4</button>
                </div>
            </div>
        </div>  
