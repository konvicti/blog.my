<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'news';
$route['catalog'] = 'pages/catalog';
$route['gallery/page/(:any)'] = 'pages/page/$1';
$route['gallery/page'] = 'pages/page';
$route['gallery'] = 'pages/gallery';
$route['message'] = 'message';
$route['blog/page/(:any)'] = 'news/page/$1';
$route['blog/page'] = 'news/page';
$route['blog/(:any)'] = 'news/test/$1';
$route['blog'] = 'news';
$route['(:any)'] = 'pages/view/$1';
$route['admin'] = 'admin';
$route['admin/add_news'] = 'admin/add_news';
$route['admin/change_news'] = 'admin/change_news';
$route['admin/delete_news'] = 'admin/delete_news';
$route['admin/edit_news'] = 'admin/edit_news';
$route['admin/changed_news'] = 'admin/changed_news';
$route['admin/add_photo'] = 'admin/add_photo';
$route['admin/log_out'] = 'admin/log_out';
$route['log'] = 'log';
$route['log/validate_credentials'] = 'log/validate_credentials';
$route['blog/add_comment'] = 'news/add_comment';
$route['form_ajax'] = 'form_ajax';
$route['form_ajax/user_login'] = 'form_ajax/user_login';
$route['form_ajax/user_email'] = 'form_ajax/user_email';
$route['form_ajax/tah_status'] = 'form_ajax/tah_status';
$route['users/registration'] = 'users/registration';
$route['users/registrate'] = 'users/registrate';
$route['users/log_in'] = 'users/log_in';
$route['users/loged_in'] = 'users/loged_in';
$route['users/log_out'] = 'users/log_out';
$route['users/login_user'] = 'form_ajax/login_user';
$route['news/check_comment'] = 'form_ajax/check_comment';






/* End of file routes.php */
/* Location: ./application/config/routes.php */