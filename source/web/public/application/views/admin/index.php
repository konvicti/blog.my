<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title ?> </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="../../../html/css/bootstrap.min.css">


        <link rel="stylesheet" href="../../../html/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../../../html/css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="../../../js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="log_block center-block">
                    <h1 class="text-center">Administrator</h1><br><br>
                    <ul>
                        <li><img src="../../../html/img/add.png" alt="add"><a href="admin/add_news">Add News</a></li>
                        <li>
                            <img src="../../../html/img/edit.png" alt="add">
                            <img src="../../../html/img/delete.png" alt="add">
                            <a href="admin/change_news">Edit / Delete News</a>
                        </li>
                    </ul>
                    <a href="/"><button type="button" class="btn btn-warning pull-left">Return to blog</button></a>
                    <a href="admin/log_out"><button type="button" class="btn btn-danger pull-right">Log out</button></a>
                </div>
            </div>
        </div>