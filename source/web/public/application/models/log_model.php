<?php

class Log_model extends CI_Model {

    public function admin_validate($data)
    {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('login', $data['login']);
        $this->db->where('password', $data['password']);
        $query = $this->db->get();

        return $query->result_Array();

    }

    public function user_login($data)
    {
        $this->db->where('login',$data['login']);
        $query = $this->db->get('users');
        return $query->result_array();

    }
    
    public function user_email($data)
    {
        $this->db->where('email',$data['email']);
        $query = $this->db->get('users');
        return $query->result_array();

    }

    public function user_add($data)
    {
        $data = array(
            'login' => $data['login'],
            'password' => $data['password'],
            'email' => $data['email']
        );

        return $this->db->insert('users', $data);
    }
    
    public function user_validate($data)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('login', $data['login']);
        $this->db->where('password', $data['password']);
        $query = $this->db->get();

        return $query->result_Array();

    }
    
};

