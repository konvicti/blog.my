<?php

class Users extends CI_Controller {

    public function registration()
    {
        $data['title'] = "Registrate new user";
        $this->load->view('users/reg_view',$data);
    }
    
    public function registrate()
    {
        $data['login'] = $this->input->post('login');
        $data['password'] = $this->input->post('password');
        $data['email'] = $this->input->post('email');
        $data['title'] = "Registrate new user";
        $data['check'] = $_POST['check'];
        
        $this->load->model('log_model');
        if (count($this->log_model->user_login($data))==1 || count($this->log_model->user_email($data))==1 || $data['check']==false)
        {
            redirect('users/registration');
        }
        else
        {
            $this->log_model->user_add($data);
            redirect('blog');
        }    
    }
    
    public function log_in()
    {
        $data['title'] = "Registrate new user";
        $this->load->view('users/log_user_view',$data);
    }
    
    public function loged_in()
    {
        $data['login'] = $this->input->post('login');
        $data['password'] = $this->input->post('password');
        $this->load->model('log_model');
        $query = $this->log_model->user_validate($data);
        $flag=false;
        foreach ($query as $row)
        {
            if ($row['login']=$data['login'] && $row['password']=$data['password']) {
                $flag=true;
            }
        }
        if ($flag)
        {
            $data = array(
                'username' => $this->input->post('login'),
                'is_loged_in' => 1
            );
            $this->session->set_userdata($data);
            redirect('/blog');
        }
        else {
            $this->index();
        }
    }    
    
    function log_out()
    {
        $data = array(
                'is_loged_in' => 0
            );
            $this->session->set_userdata($data);
            redirect($_SERVER['HTTP_REFERER']);
    }
    
}