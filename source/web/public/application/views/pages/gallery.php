<div class="col-md-10 padl0 padr0">
    <div class="fullcontent">
        <?php if (isset($user_name)): ?>
        <?php if ($user_rights==false): ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php else: ?>
        <p class="user_loged_in">You loged in like - <b><?php echo $user_name ?></b> (<a href="/users/log_out">Log out</a>)</p>
        <?php endif; ?>
        <?php else: ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php endif; ?>
        <div class="row margr0">
            <div class="col-md-7 padr0">
                <div class="content">
                    <p class="gallery__content--heading">Gallery</p>
                    <?php foreach ($gallery as $gallery_item): ?>    
                    <div class="col-lg-6">
                        <div class="gallery__imgcontainer">
                            <a href="../../../html/img/<?php echo $gallery_item['img'] ?>.png" data-lightbox="roadtrip"><img src="../../../html/img/<?php echo $gallery_item['img'] ?>_small.png" alt="img1" class="center-block"></a>
                            <p class="about__content--text"><span class="about__span--orange"><?php echo $gallery_item['title'] ?></span><br> 
                                <?php echo $gallery_item['text'] ?></p>
                        </div>
                    </div>                                    
                    <?php endforeach ?>
                    <?php if (isset($add_photo) && isset($admin_rights)): ?>
                        <?php if ($admin_rights==true && $add_photo==true): ?>                                    
                            <a href="/admin/add_photo" id="add_link">
                                <div class="col-lg-6">
                                    <div class="gallery__imgcontainer">
                                        <img src="../../../html/img/add_photo.png" class="center-block" id="add_img">
                                        <p class="add_photo_text">Add new photo</p>
                                    </div>
                                </div>
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-lg-6 margl15">
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 padl30">
                <p class="about__posts--heading text-uppercase">latest posts</p>
                <div class="about__month">
                    <div class="row">
                        <div class="col-md-1">
                            <div id="circle"></div>
                        </div>
                        <div class="col-md-10">
                            <span>January 8th, 2013</span>
                            <p class="about__content-rows--text"><span class="about__span--orange">Nam nec tellus a odio tinciduntX auctor a ornare odio.</span><br>
                                Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti ...</p>
                        </div>
                    </div>
                </div>
                <div class="about__month">
                    <div class="row">
                        <div class="col-md-1">
                            <div id="circle"></div>
                        </div>
                        <div class="col-md-10">
                            <span>January 8th, 2013</span>
                            <p class="about__content-rows--text"><span class="about__span--orange">Nam nec tellus a odio tinciduntX auctor a ornare odio.</span></p>
                        </div>
                    </div>
                </div>
                <div class="about__month">
                    <div class="row">
                        <div class="col-md-1">
                            <div id="circle"></div>
                        </div>
                        <div class="col-md-10">
                            <span>January 8th, 2013</span>
                            <p class="about__content-rows--text"><span class="about__span--orange">Nam nec tellus a odio tinciduntX auctor a ornare odio.</span><br>
                                Sed non  mauris vitae erat...</p>
                        </div>
                    </div>
                </div>
                <div class="about__month">
                    <div class="row">
                        <div class="col-md-1">
                            <div id="circle"></div>
                        </div>
                        <div class="col-md-10">
                            <span>January 8th, 2013</span>
                            <p class="about__content-rows--text"><span class="about__span--orange">Nam nec tellus a odio tinciduntX auctor a ornare odio.</span><br>
                                Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti ...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- FORM AJAX  -->                  
<div class="login_hidden">
    <form class="form-horizontal" id="user_log_form" method="post">
        <div class="form-group">
            <label for="login" class="col-sm-offset-1 col-sm-2 control-label">Login</label>
            <div class="col-sm-6">
                <input type="text" id="login" class="login form-control ntSaveForms" name="login">
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="col-sm-offset-1 col-sm-2 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" id="pass" class="password form-control" name="password">
            </div>
        </div>
        <button class="login_button btn btn-default center-block" id="login_button">Log In</button><br>                                
    </form>
    <button class="btn btn-default center-block" id="hide_button">Hide</button>
</div>

