<?php

class Form_ajax extends CI_Controller {

    public function index()
    {
        $data['log'] = $_POST['log'];
        $this->load->model('ajax_model');
        if (!count($this->ajax_model->admin_login($data))==1)
        {
            echo json_encode("Нет такого логина!");   
        }
        else
        {
            echo json_encode("");
        }
    }

    public function user_login()
    {
        $data['log'] = $_POST['log'];
        $this->load->model('ajax_model');
        if (count($this->ajax_model->user_login($data))==1)
        {
            echo json_encode("Пользователь с таким логином уже существует!");   
        }
        else
        {
            echo json_encode("");
        }
    }
    
    public function user_email()
    {
        $data['email'] = $_POST['email'];
        $this->load->model('ajax_model');
        if (count($this->ajax_model->user_email($data))==1)
        {
            echo json_encode("Пользователь с таким имейлом уже существует!");   
        }
        else
        {
            echo json_encode("");
        }
    }
    
    public function login_user()
    {
        $json = array (
            "success" => false
        );
                
        
        $data['login'] = $this->input->post('log');
        $data['password'] = $this->input->post('pass');
        $this->load->model('log_model');
        $query = $this->log_model->user_validate($data);
        $flag=false;
        foreach ($query as $row)
        {
            if ($row['login']=$data['login'] && $row['password']=$data['password']) {
                $flag=true;
            }
        }
        if ($flag)
        {
            $data = array(
                'username' => $this->input->post('log'),
                'is_loged_in' => 1
            );
            $this->session->set_userdata($data);
            $json['success'] = TRUE;                        
        }        
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));        
    }
    
    public function check_comment() {
        $json = array (
            "success" => false,
            "col" => 0
        );
        $this->load->model('news_model');
        $data['col_com'] = $this->input->post('col_com');
        $data['id'] = $this->input->post('id');        
        if (count($this->news_model->show_comment($data))>$data['col_com']) {
            $json['success'] = TRUE;
            $json['col'] = count($this->news_model->show_comment($data));
        }        
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }
    
    public function tah_status() {
        $this->load->helper('file');
        $json = array (
            "res" => ""
        );
        
        include_once 'simple_html_dom.php';        
        $html = file_get_html('http://konvicti.tahometer.com/logins');

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, 'http://konvicti.tahometer.com/logins/create?utf8=%E2%9C%93&authenticity_token='.$html->find('input[name="authenticity_token"]',0)->value.'&user%5Bemail%5D=konvicti94%40gmail.com&user%5Bpassword%5D=Joker80612%21');
	curl_setopt($curl, CURLOPT_COOKIEJAR, './files/cookie.txt');
	curl_setopt($curl, CURLOPT_COOKIEFILE, './files/cookie.txt');
	curl_setopt($curl, CURLOPT_TIMEOUT, 3);
	curl_setopt($curl, CURLOPT_FAILONERROR, 1);
	curl_setopt($curl, CURLOPT_POST, 0);
	curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5");
	curl_setopt($curl, CURLOPT_HEADER, 1);
	curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	curl_close($curl);  
        
	$html->clear(); 
 	unset($html);

	write_file('./files/tahometr.txt', $result);
	$html = file_get_html('./files/tahometr.txt');
	$json['res'] = $html->find('.canvas_td', 3)->innertext;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($json));
    }
    
}