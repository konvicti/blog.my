<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title ?> </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="../../../html/css/bootstrap.min.css">


        <link rel="stylesheet" href="../../../html/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../../../html/css/main.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="../../../js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="log_block center-block">
                    <h1 class="text-center">Registration</h1>
                    <form class="form-horizontal" id="user_reg_form" action="/users/registrate" method="post">
                        <p id="ajax_log"></p>
                        <p id="ajax_email"></p>
                        <div class="form-group">
                            <label for="login" class="col-sm-3 control-label">Login</label>
                            <div class="col-sm-6">
                                <input type="text" id="login" class="form-control ntSaveForms" name="login">
                            </div>
                            <label id="login-label" class="col-sm-3"></label>
                        </div>
                        <div class="form-group">
                            <label for="pass" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-6">
                                <input type="password" id="pass" class="form-control" name="password">
                            </div>
                            <label id="pass-label" class="col-sm-3"></label>
                        </div>
                        <div class="form-group">
                            <label for="repass" class="col-sm-3 control-label">Repeat password</label>
                            <div class="col-sm-6">
                                <input type="password" id="repass" class="form-control" name="repassword">
                            </div>
                            <img id="imgrepass" class="galka">
                            <label id="repass-label" class="col-sm-3"></label>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="email" id="email" class="form-control ntSaveForms" name="email">
                            </div>
                            <label id="email-label" class="col-sm-3"></label>
                        </div>
                        <button type="submit" class="btn btn-default center-block" id="reg_button">Registrate</button><br>
                    </form>
                    <a href="/"><button class="btn btn-default center-block">Blog</button></a>
                </div>
            </div>
        </div>
        
        
        <script type='text/javascript' src='../../../html/js/ntsaveforms.js'></script>
        <script type="text/javascript">
            var check = true;
            $('#login').focusout(function() {
                var log = $(this).val();
                $.ajax({
                    url: "<?php echo base_url('form_ajax/user_login'); ?>",
                    type: 'post',
                    data: {"log":log},
                    dataType: "json",
                    cache: false,
                    success: function(date){
                        $('#ajax_log').html(date);
                        check=false;
                    }
                });
            });
            $('#email').focusout(function() {
                var email = $(this).val();
                $.ajax({
                    url: "<?php echo base_url('form_ajax/user_email'); ?>",
                    type: 'post',
                    data: {"email":email},
                    dataType: "json",
                    cache: false,
                    success: function(date){
                        $('#ajax_email').html(date);
                        check=false;
                    }
                });
            });
            // Login
        unfocuslog = function() {
                var reg=/[\w-\W]{3,}/i;
                var spaces=/^.*\s+.*$/;
                var label = document.getElementById('login-label');
                if(!reg.test(this.value) || spaces.test(this.value) ) {
                        if ( !reg.test(this.value) ) {
                                label.innerHTML = "Логин должен сожержать от 3 символов.";
                        };
                        if ( spaces.test(this.value) ) {
                                label.innerHTML = "Пробелы - недопустимы!";
                        };
                        check=false;
                } else {
                         label.innerHTML = "";
                    }
        }
        document.getElementById('login').addEventListener('blur', unfocuslog);
                // Ïàðîëü
        unfocuspass = function() {
                var spec=/\W+/i;
                var bol =/[A-Z]+/;
                var mal =/[a-z]+/;
                var leng=/.{6,30}/i;
                var label = document.getElementById('pass-label');
                if (!spec.test(this.value) || !bol.test(this.value) || !leng.test(this.value) ) {
                        if(!spec.test(this.value) ) {
                                label.innerHTML = "Пароль должен содержать спец символы.";
                        };
                        if(!bol.test(this.value) ) {
                                label.innerHTML = "Пароль должен содержать большие буквы.";
                        };
                        if(!leng.test(this.value) ) {
                                label.innerHTML = "Длина пароля от 6 символов.";		
                        };
                        check=false;
                } else {
                                label.innerHTML = "";
                                }
        }
        document.getElementById('pass').addEventListener('blur', unfocuspass);
                //Ïîâòîð ïàðîëÿ
        unfocusrepass = function() {	 
                var label = document.getElementById('repass-label');
                var img=document.getElementById('imgrepass');
                if(this.value!=document.getElementById('pass').value) {
                        label.innerHTML = "Не совпадает.";
                        img.style.display="none";
                        check=false;
                } else {
                        img.style.display="inline";
                        img.src='../../../html/img/galka.png';
                        label.innerHTML = "";
                        }
        }
        document.getElementById('repass').addEventListener('blur', unfocusrepass);
                // Email
        unfocusemail = function() { 
                var regemail = /^[a-z0-9_\.-]+@[a-z0-9\.-]+\.[a-z\.]{2,}$/i;
                var label = document.getElementById('email-label');
                if(!regemail.test(this.value)) {
                        label.innerHTML = "Неправильно набран имейл.";
                        check=false;
                } else {
                        label.innerHTML = "";
                        }
        }
        document.getElementById('email').addEventListener('blur', unfocusemail);
        if (!check) {
            $('#user_reg_form').submit(function(){
               $.ajax({
                  url: "<?php echo base_url('users/registrate'); ?>",
                    type: 'post',
                    data: {"check":check},
                    dataType: "json",
                    cache: false
               }) 
            });
        }
        
        </script>
    </body>