<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title ?> </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="../../../html/css/bootstrap.min.css">


        <link rel="stylesheet" href="../../../html/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../../../html/css/main.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="../../../js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="log_block center-block">
                    <h1 class="text-center">Administrator</h1>
                    <form class="form-horizontal" action="/log/validate_credentials" method="post">
                        <div class="form-group">
                            <label for="login" class="col-sm-offset-1 col-sm-2 control-label">Login</label>
                            <div class="col-sm-6">
                                <input type="text" id="login" class="form-control" name="login">
                            </div>
                            <label for="" id="pp" class="col-sm-3"></label>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-offset-1 col-sm-2 control-label">Password</label>
                            <div class="col-sm-6">
                                <input type="password" id="password" class="form-control" name="password">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-default center-block">Log In</button><br>
                    </form>
                    <a href="/"><button class="btn btn-default center-block">Blog</button></a>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('#login').focusout(function() {
                var log = $(this).val();
                $.ajax({
                    url: "<?php echo base_url('form_ajax'); ?>",
                    type: 'post',
                    data: {"log":log},
                    dataType: "json",
                    cache: false,
                    success: function(date){
                        $('#pp').html(date);
                    }
                });
            });
        </script>
    </body>