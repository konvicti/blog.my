<div class="col-md-10 padl0 padr0">
    <div class="fullcontent">
        <?php if (isset($user_name)): ?>
        <?php if ($user_rights==false): ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php else: ?>
        <p class="user_loged_in">You loged in like - <b><?php echo $user_name ?></b> (<a href="/users/log_out">Log out</a>)</p>
        <?php endif; ?>
        <?php else: ?>
        <div class="log_reg_div">
            <button class="btn btn-warning" id="log_btn">Log in</button>
            <a href="/users/registration"><button class="btn btn-warning" id="reg_btn">Registration</button></a>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-7 padr0">
                <div class="content">
                    <p class="content--heading"><?php echo $news_item['title'] ?></p>
                    <?php if (!empty($news_item['img'])): ?>
                    <img src="<?php echo $news_item['img'] ?>"  alt="Responsive image" class="img-responsive center-block">
                    <?php endif; ?>
                    <p class="content--text"><?php echo $news_item['text'] ?></p>
                    <a href="/blog"><button class="btn btn-warning">Return</button></a>
                </div>
            </div>
            <div class="col-md-3 padl0">
                <div class="month">
                    <div id="circle"></div>
                    <span>January 8th, 2013</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 padr0">
                <div class="content comment">
                    <h1 class="comment__heading">Comments</h1>
                    <?php if (isset($user_rights)): ?>
                    <?php if ($user_rights==true): ?>
                    <?php $i=1; ?>
                    <?php if (empty($comment)): ?>
                        <p class="comment__no--text">There are no comments for now...</p>
                    <?php endif; ?>                    
                    <?php foreach ($comment as $comment_item): ?>
                        <div class="each__comment">
                            <p class="comment__name"><span class="num_comment">#<?= $i ?></span><?php echo $comment_item['yourname'] ?></p>
                            <p class="comment__text"><?php echo $comment_item['text'] ?></p>
                            <hr class="hr__date">
                            <p class="comment__date"><?= $comment_item['date_time'] ?></p>
                        </div>
                        <?php $i++ ?>
                    <?php endforeach ?>
                    <hr class="soften">
                    <form action="add_comment" method="post">
                        <div class="form-group">
                            <label for="news_text" class="comment_new">Type your comment bellow:</label>
                            <textarea class="form-control" rows="3" name="text" id="news_text"></textarea>
                        </div>
                        <input type="text" name="slug" value="<?php echo $news_item['slug'] ?>" id="slug_invis">
                        <input type="text" name="id_news" value="<?php echo $id ?>" id="id_invis">
                        <button type="submit" class="btn btn-default" name="submit">Send comment</button>
                    </form>                  
                    <?php else: ?>
                    <p class="not_reg_user">Only loged in users can watch and comment these news!</p>
                    <?php endif; ?>
                    <?php else: ?>
                    <p class="not_reg_user">Only loged in users can watch and comment these news!</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
<!-- FORM AJAX  -->                  
<div class="login_hidden">
    <form class="form-horizontal" id="user_log_form" method="post">
        <div class="form-group">
            <label for="login" class="col-sm-offset-1 col-sm-2 control-label">Login</label>
            <div class="col-sm-6">
                <input type="text" id="login" class="login form-control ntSaveForms" name="login">
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="col-sm-offset-1 col-sm-2 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" id="pass" class="password form-control" name="password">
            </div>
        </div>
        <button class="login_button btn btn-default center-block" id="login_button">Log In</button><br>                                
    </form>
    <button class="btn btn-default center-block" id="hide_button">Hide</button>
</div>                