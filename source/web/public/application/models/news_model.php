<?php

class News_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_news($num, $offset) {
        $slug=False;
        if ($slug === FALSE) {
            $query = $this->db->get('blog_news', $num, $offset);
            return $query->result_array();
        }

        $query = $this->db->get_where('blog_news', array('slug' => $slug));
        return $query->row_array();
    }
    
    public function get_each($slug=False) {
        
        if ($slug === FALSE) {
            $query = $this->db->get('blog_news', $num, $offset);
            return $query->result_array();
        }

        $query = $this->db->get_where('blog_news', array('slug' => $slug));
        return $query->row_array();
    }


    public function add_news($data) {
        $this->load->helper('url');

        $slug = url_title($this->input->post('title'), 'dash', TRUE);

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'text' => $this->input->post('text')
        );

        return $this->db->insert('blog_news', $data);
    }
    
    public function get_news_to_del($slug=False) {
        
        if ($slug === FALSE) {
            $query = $this->db->get('blog_news');
            return $query->result_array();
        }

        $query = $this->db->get_where('blog_news', array('slug' => $slug));
        return $query->row_array();
    }

    
    public function del_news($data) {
        $this->db->where('slug', $data['slug']);
        $this->db->delete('blog_news');
    }
    
    public function get_edit_news($data) {
        $this->db->where('slug',$data['slug']);
        $query = $this->db->get('blog_news');
        return $query->result_array();
    }
    
    public function changed_news($data) {
        $this->load->helper('url');

        $slug = url_title($this->input->post('title'), 'dash', TRUE);

        $datal = array(
            'title' => $data['titled'],
            'text' => $data['text']
        );
        $this->db->where('id',$data['id']);
        return $this->db->update('blog_news', $datal);
    }
    
    public function add_comment($data) {

        $datal = array(
            'id_news' => $data['id_news'],
            'yourname' => $data['user_name'],
            'text' => $data['text']
        );
        
        return $this->db->insert('comments', $datal);
    }
    
    public function show_comment($data) {

        $this->db->where('id_news',$data['id']);
        $query = $this->db->get('comments');
        return $query->result_array();
        
    }
    
    public function get_gallery($num, $offset) {
        $query = $this->db->get('gallery', $num, $offset);
        return $query->result_array();
    }
    
    public function add_photo($data) {
        $this->load->helper('url');

        $slug = url_title($this->input->post('title'), 'dash', TRUE);

        $data = array(
            'img' => $this->input->post('img'),
            'title' => $this->input->post('title'),            
            'text' => $this->input->post('text')
        );

        return $this->db->insert('gallery', $data);
    }

};


