<?php

class Message extends CI_Controller {

    public function index() {

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Contact';
        $data['menu'] = 'contact';
        $data['first_name'] = $this->input->post('firstname');
        $data['last_name'] = $this->input->post('lastname');
        $data['phone'] = $this->input->post('firstname');
        $data['email'] = $this->input->post('email');
        $data['message'] = $this->input->post('message');
        
        $this->load->model('message_model');
        $this->message_model->add_message($data);
        redirect('/');
    }
   
}
