<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title ?> </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="../../../html/css/bootstrap.min.css">


        <link rel="stylesheet" href="../../../html/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../../../html/css/main.css">
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="../../../js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="fullcontent">
                        <div class="content">
                            <h1 class="text-center">Add Photo</h1>
                            <?php echo validation_errors(); ?>

                            <?php echo form_open('admin/add_photo') ?>
                                <div class="form-group">
                                    <label for="img">Type path of IMG</label><input type="text" id="img" class="form-control" name="img">
                                </div>
                                <div class="form-group">
                                    <label for="title">Type title</label><input type="text" id="title" class="form-control" name="title">
                                </div>

                                <div class="form-group">
                                    <label for="photo_textarea">Type text</label>
                                    <textarea class="form-control" rows="10" name="text" id="photo_textarea" name="text"></textarea>
                                </div>
                            
                                <button type="submit" class="btn btn-default" name="submit">Submit</button>                            
                                <?php if ($flag): ?>
                                <div class="success_form">
                                    <p>Adding photo was successful</p>
                                    <button class="button_ok">OK</button>
                                </div>
                                <?php endif; ?>
                            </form>
                            <a href="/gallery/page/<?php echo $num_page ?>"><button class="btn btn-warning" id="btn-return">Return</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../../../../html/js/vendor/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
    <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
    <script src="../../../../html/js/main.js"></script>
    </body>
</html>