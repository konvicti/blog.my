<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title ?> </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="stylesheet" href="../../../html/css/bootstrap.min.css">


        <link rel="stylesheet" href="../../../html/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../../../html/css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="../../../js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="fullcontent">
                        <a href="/admin"><button class="btn btn-warning" id="btnreturn">Return to administration</button></a>
                        <?php foreach ($news as $news_item): ?>
                        <div class="row">
                            <div class="col-md-7 padr0">
                                <div class="content">
                                    <p class="content--heading"><?php echo $news_item['title'] ?></p>
                                    <?php if (!empty($news_item['img'])): ?>
                                    <img src="<?php echo $news_item['img'] ?>"  alt="Responsive image" class="img-responsive center-block">
                                    <?php endif; ?>
                                    <p class="content--text"><?php echo $news_item['prev'] ?></p>
                                    <a href="delete_news?slug=<?=$news_item['slug']?>"><img src="../../../html/img/delete.png" alt="delete" class="delete_news"></a>
                                    <a href="edit_news?slug=<?=$news_item['slug']?>"><img src="../../../html/img/edit.png" alt="delete" class="edit_news"></a>
                                </div>
                            </div>
                            <div class="col-md-3 padl0">
                                <div class="month">
                                    <div id="circle"></div>
                                    <span>January 8th, 2013</span>
                                </div>
                            </div>
                        </div>                      

                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
                        
                        
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>');</script>
    <script src="../../../../html/js/vendor/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
    <script src="../../../../html/js/main.js"></script>
    </body>
</html>